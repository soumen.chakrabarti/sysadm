#! /usr/bin/python3

import os
import smbus
import time


def get_temp_humid_zs05(bus, address):
    buf = bus.read_i2c_block_data(address, 0x00, 5)
    humidity = float(f"{buf[0]}.{buf[1]}")
    temperature = float(f"{buf[2]}.{buf[3]}")
    return (temperature, humidity)


if __name__ == "__main__":
    bus = smbus.SMBus(1)
    address = 0x5c
    tmpout = "/tmp/envsens_zs05.tsv"
    out = "/var/log/envsens_zs05.tsv"
    while True:
        try:
            now = int(time.time())
            (temp, humid) = get_temp_humid_zs05(bus, address)
            with open(tmpout, 'w') as file:
                file.write("{:d}\t{:.1f}\t{:.1f}\n".format(now, temp, humid))
            os.replace(tmpout, out)  # atomic file switcharoo
        except OSError:
            pass
        time.sleep(10)

