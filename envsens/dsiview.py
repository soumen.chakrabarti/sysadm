#! /usr/bin/python3

import argparse
import time
from datetime import datetime
import contextlib
with contextlib.redirect_stdout(None):
    import pygame


def get_text_content(toggle: bool):
    hours = datetime.now().strftime('%H')
    minutes = datetime.now().strftime('%M')
    time_str = hours + (":" if toggle else " ") + minutes
    envsens = "/var/log/envsens_htu21d.tsv"
    (timethen, temp_str, humid_str) = open(envsens, 'r').read().split("\t")
    if time.time() > int(timethen) + 20:
        return [time_str, "----", "----"]
    temp = float(temp_str)
    humid = float(humid_str)
    humid_str = "{:.1f} %".format(humid)
    temp_str = "{:.1f}°C".format(temp)
    return [time_str, humid_str, temp_str]


ap = argparse.ArgumentParser()
ap.add_argument("--full", action="store_true", help="full screen")
av = ap.parse_args()

pygame.init()
if av.full:
    screen_info = pygame.display.Info()
    screen_width = screen_info.current_w
    screen_height = screen_info.current_h
    window = pygame.display.set_mode((screen_width, screen_height), pygame.FULLSCREEN | pygame.NOFRAME)
else:
    pygame.display.set_caption("EnvSens")
    window_width = 800
    window_height = 480
    window = pygame.display.set_mode((window_width, window_height)) #, pygame.RESIZABLE)

background_color = (0, 0, 0)  # Black
window.fill(background_color)
font = pygame.font.SysFont("Inconsolata", 180)
text_color = (255, 255, 255)  # White

# Main loop
toggle = True
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.VIDEORESIZE:
            window_width, window_height = event.size
            window = pygame.display.set_mode((window_width, window_height), pygame.RESIZABLE)

    window.fill(background_color)  # Black
    text_content = get_text_content(toggle)
    # Render and position the text
    line_height = 150
    for i, line in enumerate(text_content):
        text_surface = font.render(line, True, text_color)
        current_width = pygame.display.get_surface().get_width()
        text_rect = text_surface.get_rect(center=(current_width // 2, 75 + i * line_height))
        window.blit(text_surface, text_rect)
    # Update the window display
    pygame.display.update()
    pygame.time.wait(500)
    toggle = not toggle
    

# Quit pygame
pygame.quit()

