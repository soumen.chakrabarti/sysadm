#! /usr/bin/python3

from collections import Counter, defaultdict
import csv
import glob
import pwd
import re
import matplotlib as mpl
import matplotlib.pyplot as plt


def load_quota_snapshot(qpath: str):
    name_to_bytes = Counter()
    with open(qpath) as zqf:
        for row in csv.reader(zqf, delimiter='\t'):
            usernom = row[1]
            try:
                if re.match(r'\d+', usernom):
                    pwdline = pwd.getpwuid(int(usernom))
                else:
                    pwdline = pwd.getpwnam(usernom)
                occupied = int(row[2])
                username = pwdline.pw_name
                name_to_bytes[username] = occupied
            except KeyError as ke:
                pass
    return name_to_bytes


def remove_prefix(pref :str, src :str):
    if src.startswith(pref):
        return src[len(pref):]
    else:
        return src


if __name__ == "__main__":
    prefix = "/var/log/zfs/zfsquota-"
    qdates = list()
    qcounters = list()
    qusers = set()
    for qpath in sorted(glob.glob(prefix + "*")):
        qdate = remove_prefix(prefix, qpath)
        qdates.append(qdate)
        qcnt = load_quota_snapshot(qpath)
        qcounters.append(qcnt)
        qusers.update(qcnt.keys())
    userseries = dict()
    for quser in qusers:
        userseries[quser] = list()
    for qcnt in qcounters:
        for quser in qusers:
            if quser in qcnt:
                userseries[quser].append(qcnt[quser])
            else:
                userseries[quser].append(0)

    # trace of total through time
    qtotals = [sum(qcounter.values()) for qcounter in qcounters]
    print(qdates, qtotals)
    fig, ax = plt.subplots()
    plt.gca().xaxis.set_major_locator(mpl.dates.AutoDateLocator())
    ax.plot(qdates, qtotals)
    plt.xticks(rotation = 90)
    plt.show()
    
    # reorder quser in decreasing average occupancy order
    user_to_avg = Counter()
    for user, series in userseries.items():
        user_to_avg[user] = sum(series) / len(series)
    user_to_top_avg = user_to_avg.most_common(10)
    filtered_userseries = { user : userseries[user] for (user, avgbytes) in user_to_top_avg }

    fig, ax = plt.subplots()
    plt.gca().xaxis.set_major_locator(mpl.dates.AutoDateLocator())
    ax.stackplot(qdates, *(filtered_userseries.values()), labels=filtered_userseries)
    ax.legend(loc='upper left')
    plt.xticks(rotation = 90)
    plt.show()
    
    # sumbytes = sum(quotasnap.values())
    # prefix_fraction = 0
    # for (name, bytes) in quotasnap.most_common():
    #     prefix_fraction += 1.*bytes/sumbytes
    #     if prefix_fraction > 0.95:
    #         break
    #     print(bytes, name)
