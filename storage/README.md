Code for various storage management actions:
* Detect ZFS physical device failures and control bay LEDs in enclosures accordingly.
* Detect (not necessarily ZFS) devices and their SMART status and print in a table.
* Check ZFS quota over time (assuming a log has been saved) and draw informative charts.


